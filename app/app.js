/**
 * app.js
 *
 * This is the entry file for the application, only setup and boilerplate
 * code.
 */

// Needed for redux-saga es6 generator support
import 'babel-polyfill';

// Import all the third party stuff
import React from 'react';
import ReactDOM from 'react-dom';
import uplink from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import { Provider } from 'react-redux';
import { applyRouterMiddleware, Router, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import { useScroll } from 'react-router-scroll';
// import 'sanitize.css/sanitize.css';

// Importing Project related CSS
// import 'assets/css/bootstrap-theme.min.css';
import 'assets/css/bootstrap.min.css';
import 'assets/css/font-awesome.min.css';
import 'assets/css/style.css';
import 'assets/css/custom.css';
// import 'assets/css/style_color.css';
// import 'app.css';

// Import root app
import App from 'containers/App';

// Import selector for `syncHistoryWithStore`
import { makeSelectLocationState } from 'containers/App/selectors';

// Import Language Provider
import LanguageProvider from 'containers/LanguageProvider';

// Load the favicon, the manifest.json file and the .htaccess file
/* eslint-disable import/no-unresolved, import/extensions */
import '!file-loader?name=[name].[ext]!./favicon.ico';
import '!file-loader?name=[name].[ext]!./manifest.json';
import 'file-loader?name=[name].[ext]!./.htaccess';
/* eslint-enable import/no-unresolved, import/extensions */

import configureStore from './store';

// Import i18n messages
import { translationMessages } from './i18n';

// Import CSS reset and Global Styles
import initStyles from './global-styles';

// Import root routes
import createRoutes from './routes';

const storeConfigured = (store) => {
// Sync history and store, as the react-router-redux reducer
// is under the non-default key ("routing"), selectLocationState
// must be provided for resolving how to retrieve the "route" in the state
  const history = syncHistoryWithStore(browserHistory, store, {
    selectLocationState: makeSelectLocationState(),
  });

// Set up the router, wrapping all Routes in the App component
  const rootRoute = {
    component: App,
    childRoutes: createRoutes(store),
  };

  const render = (messages) => {
    initStyles().then(() => {
      ReactDOM.render(
        <Provider store={store}>
          <LanguageProvider messages={messages}>
            <Router
              history={history}
              routes={rootRoute}
              render={
              // Scroll to top when going to a new page, imitating default browser
              // behavior
              applyRouterMiddleware(useScroll())
            }
            />
          </LanguageProvider>
        </Provider>,
      document.getElementById('app')
     );
    });
  };

// Hot reloadable translation json files
  if (module.hot) {
  // modules.hot.accept does not accept dynamic dependencies,
  // have to be constants at compile-time
    module.hot.accept('./i18n', () => {
      translationMessages().then(render);
    });
  }

// Chunked polyfill for browsers without Intl support
  if (!window.Intl) {
    (new Promise((resolve) => {
      resolve(import('intl'));
    }))
    .then(() => Promise.all([
      import('intl/locale-data/jsonp/en.js'),
    ]))
    .then(() => translationMessages().then(render))
    .catch((err) => {
      throw err;
    });
  } else {
    translationMessages().then(render);
  }
};

// Create redux store with history
// this uses the singleton browserHistory provided by react-router
// Optionally, this could be changed to leverage a created history
// e.g. `const browserHistory = useRouterHistory(createBrowserHistory)();`
const initialState = {};
configureStore(initialState, browserHistory)((store) => {
  uplink.init({
    client: process.env.ORBIT_IDENTITY_CLIENT,
    username: process.env.ORBIT_IDENTITY_USER,
    password: process.env.ORBIT_IDENTITY_PASSWORD,
    realm: process.env.ORBIT_IDENTITY_REALM,
    identity: process.env.ORBIT_IDENTITY,
    baseStation: process.env.UPLINK_ENDPOINT,
  }).then(() => storeConfigured(store))
    .catch(console.error); // eslint-disable-line
});

// Install ServiceWorker and AppCache in the end since
// it's not most important operation and if main code fails,
// we do not want it installed
if (process.env.NODE_ENV === 'production') {
  require('offline-plugin/runtime').install(); // eslint-disable-line global-require
}
