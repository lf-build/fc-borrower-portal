import isInRange from 'lib/validation/isInRange';
import isRegexMatch from 'lib/validation/isRegexMatch';
import { emailRegex, mobileRegex, passwordRegex, textRegex } from 'lib/validation/regexList';

const validateForm = (that, _values) => {
  const errors = {};
  const values = _values.toJS();

  // Next button validations
  if (!values.requestedAmount) {
    errors.requestedAmount = 'Requested Amount is required';
  }

  if (!values.firstName) {
    errors.firstName = 'First Name is required';
  } else if (!isInRange(values.firstName, 2, 100)) {
    errors.firstName = 'Field length must be between 2 and 100';
  } else if (!isRegexMatch(values.firstName.trim(), textRegex)) {
    errors.firstName = 'Numbers or special characters not allowed';
  }

  if (!values.lastName) {
    errors.lastName = 'Last Name is required';
  } else if (!isInRange(values.lastName, 2, 100)) {
    errors.lastName = 'Field length must be between 2 and 100';
  } else if (!isRegexMatch(values.lastName.trim(), textRegex)) {
    errors.lastName = 'Numbers or special characters not allowed';
  }

  if (!values.loanTimeFrame) {
    errors.loanTimeFrame = 'Loan time frame is required';
  }

  if (!values.purposeOfLoan) {
    errors.purposeOfLoan = 'Purpose of loan is required';
  }
  if (!values.primaryPhone) {
    errors.primaryPhone = 'Primary Phone is required';
  } else if (!isRegexMatch(values.primaryPhone.replace(/ /g, ''), mobileRegex)) {
    errors.primaryPhone = 'Invalid Primary Phone';
  }
  if (!values.emailAddress) {
    errors.emailAddress = 'Email Address is required';
  } else if (!isRegexMatch(values.emailAddress, emailRegex)) {
    errors.emailAddress = 'Invalid Email Address';
  }
/*
  if (!values.dateNeeded) {
    errors.dateNeeded = 'Date needed is required';
  } else if (!isRegexMatch(values.dateNeeded, dateRegex)) {
    errors.dateNeeded = 'Invalid Date';
  } else {
    const parts = values.dateNeeded.split('/');
    const mydate = new Date(parts[2], parts[0] - 1, parts[1]);
    if (mydate <= new Date()) {
      errors.dateNeeded = 'Date needed can not be a past date';
    }
  }*/

  if (!values.password) {
    errors.password = 'Password is required';
  } else if (!isRegexMatch(values.password, passwordRegex)) {
    errors.password = 'Password must be eight characters including one uppercase letter, one special character and alphanumeric characters';
  }
  if (!values.confirmPassword) {
    errors.confirmPassword = 'Confirm Password is required';
  } /* else if (!isRegexMatch(values.confirmPassword, passwordRegex)) {
    errors.confirmPassword = 'Confirm password must be eight characters including one uppercase letter, one special character and alphanumeric characters';
  }*/

  if (values.password && values.confirmPassword && values.password !== values.confirmPassword) {
    errors.confirmPassword = 'Confirm password is not the same as password';
  }

  return errors;
};

export default validateForm;
