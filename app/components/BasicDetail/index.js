/**
*
* BasicDetail
*
*/

import React from 'react';
import lodash from 'lodash';
import Spinner from 'components/Spinner';
import { Field, reset } from 'redux-form/immutable';
import Form from '@ui/Form';
import TextField from '@ui/TextField';
import PhoneField from '@ui/PhoneField';
import localityNormalizer from '@ui/Utils/localityNormalizer';
import decodeEntity from 'lib/decodeEntity';
import messages from './messages';

import validateForm from './validate';

const ButtonPanel = (props) => {
  const { submitting ,resetForm} = props; // eslint-disable-line
  return (
    <div className="form-btn pull-right">
      <input type="button" disabled={submitting} onClick={() => resetForm()} className="btn submit-btn" value="Cancel" />
      <span>&nbsp;</span>
      <input type="submit" disabled={submitting} className="btn submit-btn" value="Next" />
    </div>
  );
};

const DropDownOptions = (obj) => (
  <option key={obj.key} value={obj.key}>{decodeEntity(obj.value)}</option>
  );

const DropDownList = ({ input, label, options, type, meta: { touched, error, warning } }) => ( // eslint-disable-line
  <div className=" form-group">
    <label htmlFor="label3">{label}</label>
    <select className="select_fields" {...input} placeholder={label} type={type}>
      <option key="0"></option>
      {options && Object.entries(options).map(([key, value]) => ({ key, value })).map(DropDownOptions)}
    </select>
    {touched &&
        ((error && <span className="error_message">{error}</span>) ||
          (warning && <span>{warning}</span>))}
  </div>
);

class BasicDetail extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { showSpinner: false };
  }

  loanRange={ 10000: '$5,000-$10,000',
    25000: '$10,000-$25,000',
    50000: '$25,000-$50,000',
    100000: '$50,000-$100,000',
    250000: '$100,000-$250,000',
    500000: '$250,000-$500,000',
    750000: '$500,000-$750,000',
    1000000: '$750,000-$1,000,000' };

  modifyJson = (values) => {
    const finalValues = {
      ...values,
    };
    if (values && values.primaryPhone) {
      finalValues.primaryPhone = values.primaryPhone.replace('(', '').replace(')', '').replace(/-/g, '').replace(/ /g, '');
    }
    if (values && values.requestedAmount) {
      if (isNaN(values.requestedAmount)) {
        if (values.requestedAmount.indexOf('$') !== -1 && values.requestedAmount.indexOf(',') !== -1) {
          finalValues.requestedAmount = parseInt(values.requestedAmount.replace(/[$, ]/g, ''), 10);
        }
      } else {
        finalValues.requestedAmount = values.requestedAmount;
      }
    }
    // Remove null, undefined from payload object
    const payload = lodash.pickBy(this.props.payloadGenerator(finalValues, 'basic'), lodash.identity);
    if (payload.BusinessTaxID) {
      payload.BusinessTaxID = payload.BusinessTaxID.replace('-', '');
    }
    payload.ClientIpAddress = this.props.ip;
    this.setState({ showSpinner: true });
    return payload;
  };

  afterSubmit=(err, data) => {
    this.setState({ showSpinner: false });
    this.props.updateStateAfterSubmit(err, data, 'basic');
  }
  initialValues = () => {
    const returnValue = {};
    if (this.props.basic) {
      return this.props.basic;
    }
    return returnValue;
  };
  handleOnLoadError = () => {
   // do not add authCheck here
  };
  resetForm = () => {
    this.props.dispatch(reset('basicDetails'));
  };
  render() {
    const parsedMessages = localityNormalizer(messages);
    return (
      <section id="content1" className="tab-section">
        <Form
          passPropsTo="ButtonPanel"
          afterSubmit={(err, data) => this.afterSubmit(err, data)}
          initialValuesBuilder={() => this.initialValues()}
          payloadBuilder={(values) => this.modifyJson(values)}
          validate={(values) => validateForm(this, values)}
          name="basicDetails"
          action="api/ca/save-lead"
          onLoadError={this.handleOnLoadError}
        >
          <div className="comman_application_wraper">
            <div className="business-details">
              <div className="col-md-6 col-sm-6">
                <Field label="How much do you need?" className="form-control" type="select" component={DropDownList} options={this.loanRange} name="requestedAmount" />
              </div>
              <div className="col-md-6 col-sm-6">
                <Field label="How soon do you need it?" name="loanTimeFrame" options={this.props.lookup.LoanTimeFrame} component={DropDownList} type="select" />
              </div>
              <div className="col-md-6 col-sm-6">
                <Field label="Purpose of funds?" name="purposeOfLoan" options={this.props.lookup.PurposeOfLoan} component={DropDownList} type="select" />
              </div>
              <div className="col-md-6 col-sm-6">
                <TextField className="form-control" name="firstName" {...parsedMessages.firstName} />
              </div>
              <div className="col-md-6 col-sm-6">
                <TextField className="form-control" name="lastName" {...parsedMessages.lastName} />
              </div>
              <div className="col-md-6 col-sm-6">
                <PhoneField className="form-control" name="primaryPhone" {...parsedMessages.primaryPhone} />
              </div>
            </div>
            <div className="business-details">
              <div className="col-md-6 col-sm-6">
                <TextField disabled={this.props.disableFields} className="form-control" name="emailAddress" {...parsedMessages.emailAddress} />
              </div>
              <div className="col-md-6 col-sm-6">
                <TextField disabled={this.props.disableFields} type="password" className="form-control" name="password" {...parsedMessages.password} />
              </div>
              <div className="col-md-6 col-sm-6">
                <TextField disabled={this.props.disableFields} type="password" className="form-control" name="confirmPassword" {...parsedMessages.confirmPassword} />
              </div>
              { /*  <div className="col-md-6 col-sm-6">
                <DateField
                  dateFormat="mm/dd/yyyy"
                  name="dateNeeded"
                  maxYears={100}
                  {...parsedMessages.dateNeeded}
                /></div>*/}
            </div>
            <div className="business-details">
            </div>
            <div className="comman_applicationinner business-details">
              <div className="col-md-6 col-sm-6">{this.state.showSpinner && <Spinner />}</div>
              <ButtonPanel resetForm={this.resetForm} />
            </div>
          </div>
        </Form>
      </section>
    );
  }
}

BasicDetail.propTypes = {
  dispatch: React.PropTypes.func.isRequired,
  payloadGenerator: React.PropTypes.any,
  basic: React.PropTypes.any,
  lookup: React.PropTypes.any,
  updateStateAfterSubmit: React.PropTypes.any,
  disableFields: React.PropTypes.any,
  ip: React.PropTypes.string,
};

export default BasicDetail;
