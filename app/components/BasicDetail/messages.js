/*
 * BasicDetail Messages
 *
 * This contains all the text for the BasicDetail component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.BasicDetail.header',
    defaultMessage: 'This is the BasicDetail component !',
  },
  firstNameLabel: {
    id: 'app.components.BasicDetail.firstNameLabel',
    defaultMessage: 'First Name',
  },
  firstNamePlaceholder: {
    id: 'app.components.BasicDetail.firstNamePlaceholder',
    defaultMessage: 'First Name',
  },
  lastNameLabel: {
    id: 'app.components.BasicDetail.lastNameLabel',
    defaultMessage: 'Last Name',
  },
  lastNamePlaceholder: {
    id: 'app.components.BasicDetail.lastNamePlaceholder',
    defaultMessage: 'Last Name',
  },
  primaryPhoneLabel: {
    id: 'app.components.BasicDetail.primaryPhoneLabel',
    defaultMessage: 'Primary Phone',
  },
  primaryPhonePlaceholder: {
    id: 'app.components.BasicDetail.primaryPlaceholder',
    defaultMessage: 'Primary Phone #',
  },
  passwordLabel: {
    id: 'app.components.BasicDetail.passwordLabel',
    defaultMessage: 'Password',
  },
  passwordPlaceholder: {
    id: 'app.components.BasicDetail.passwordPlaceholder',
    defaultMessage: 'Password',
  },
  confirmPasswordLabel: {
    id: 'app.components.BasicDetail.confirmPasswordLabel',
    defaultMessage: 'Confirm Password',
  },
  confirmPasswordPlaceholder: {
    id: 'app.components.BasicDetail.confirmPasswordPlaceholder',
    defaultMessage: 'Confirm Password',
  },
  emailAddressLabel: {
    id: 'app.components.BasicDetail.emailAddressLabel',
    defaultMessage: 'Email Address',
  },
  emailAddressPlaceholder: {
    id: 'app.components.BasicDetail.emailAddressPlaceholder',
    defaultMessage: 'Email Address',
  },
  requestedAmountLabel: {
    id: 'app.components.BasicDetail.requestedAmountLabel',
    defaultMessage: 'Requested Amount',
  },
  requestedAmountPlaceholder: {
    id: 'app.components.BasicDetail.requestedAmountPlaceholder',
    defaultMessage: 'Requested Amount',
  },
  dateNeededLabel: {
    id: 'app.components.BasicDetail.dateNeededLabel',
    defaultMessage: 'Date Needed',
  },
  dateNeededPlaceholder: {
    id: 'app.components.BasicDetail.dateNeededPlaceholder',
    defaultMessage: 'Date Needed',
  },
});
