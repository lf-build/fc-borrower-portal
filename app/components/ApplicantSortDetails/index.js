/**
*
* ApplicantSortDetails
*
*/

import React from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router';
import { createStructuredSelector } from 'reselect';
import redirectWithReturnURL from 'components/Helper/redirectWithReturnURL';
import { phoneMasking } from 'components/Helper/formatting';
import { injectIntl, intlShape, FormattedNumber } from 'react-intl';
import Spinner from 'components/Spinner';
import { loadUserProfile } from 'sagas/auth/actions';
import makeAuthSelector from 'sagas/auth/selectors';
import messages from './messages';

class ApplicantSortDetails extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { applicationNumber: (this.props.auth && this.props.auth.user && this.props.auth.user.applicationNumber) ? this.props.auth.user.applicationNumber : undefined, currentTab: 1 };
  }
  componentDidMount() {
    if (this.state.applicationNumber === undefined) {
      redirectWithReturnURL(this.props.location.pathname);
    } else {
      this.props.dispatch(loadUserProfile({ userId: this.props.auth.user.userId }));
    }
  }
  render() {
    const { appMode, auth: { user } } = this.props;
    if (!appMode) {
      return (<span />);
    }
    if (!user) {
      return <Spinner />;
    }
    const phoneNumber = phoneMasking(user.phoneNumber);
    const { formatMessage } = this.props.intl;
    return (
      <div className="main-header clearfix">
        <Link className="more-link" to="/overview"><i className="fa fa-angle-double-right"></i></Link>
        <div className="page-title pull-left">
          <h3 className="no-margin">{user.userName}</h3>
          <span><i className="fa fa-file-text-o"></i>{formatMessage(messages.app)}#:{user.applicationNumber}</span>
          <span><i className="fa fa-phone"></i>{phoneNumber}</span>
          <span><i className="fa fa-envelope"></i>{user.email}</span>
        </div>
        <ul className="page-stats">
          <li>
            <div className="value">
              <span>{formatMessage(messages.loanApplied)}</span>
              <h4 id="currentVisitor"><FormattedNumber {...{ value: user.loanAmount, style: 'currency', currency: 'USD', maximumFractionDigits: 2, minimumFractionDigits: 0 }} /></h4>
            </div>
          </li>
          <li>
            <div className="value">
              <span>{formatMessage(messages.purposeOfFund)}</span>
              <h4><strong id="currentBalance">{user.loanPurpose}</strong></h4>
            </div>
          </li>
        </ul>
      </div>
    );
  }
}

ApplicantSortDetails.propTypes = {
  appMode: React.PropTypes.bool,
  auth: React.PropTypes.object,
  intl: intlShape.isRequired,
  location: React.PropTypes.object.isRequired,
  dispatch: React.PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  auth: makeAuthSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(withRouter(ApplicantSortDetails)));
