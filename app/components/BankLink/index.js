/**
*
* BankLink
*
*/

import React from 'react';
import { Link, withRouter } from 'react-router';
import { connect } from 'react-redux';
import Uploader from 'components/Uploader';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import authCheck from 'components/Helper/authCheck';
import { injectIntl, intlShape } from 'react-intl';
import Spinner from 'components/Spinner';
import messages from './messages';

class BankLink extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { applicationNumber: this.props.applicationNumber, collapse: 'in', collapseArrow: 'collapsed', hideMessage: true };
  }
  componentDidMount() {
    window.applicationNumber = this.state.applicationNumber;
    this.handler = Plaid.create({ // eslint-disable-line
      clientName: 'broker portal',
      env: 'tartan',
      key: 'test_key', // Replace with your public_key to test with live credentials
      product: ['connect'],
    // webhook: '[WEBHOOK_URL]', // Optional – use webhooks to get transaction and error updates
      selectAccount: true, // Optional – trigger the Select Account
      onLoad: () => {
      // Optional, called when Link loads
      },
      onSuccess : (publicToken, metadata)=> { // eslint-disable-line
        const payload = metadata;
        payload.applicationNumber = window.applicationNumber;
        execute(undefined, undefined, ...'api/account-linking/set-bank-account'.split('/'), payload)
           .then(() => {
             setTimeout(() => this.loadData(), 5000);
           }).catch((e) => authCheck(this.props.dispatch)(e));
      },
      onExit : (err, metadata) => { // eslint-disable-line
        if (err != null) {
        // todo
        }
      },
    });
    this.loadData();
  }
  loadData = () => {
    const payload = { applicationNumber: this.state.applicationNumber };
    execute(undefined, undefined, ...'api/ca/get-accounts'.split('/'), payload)
           .then((response) => {
             this.setState({
               accountDetails: response.body.accounts,
               voidedCheck: response.body.voidedCheck,
               hideMessage: ((response.body.accounts && response.body.accounts.accountNumber) || (response.body.voidedCheck && response.body.voidedCheck.files && (response.body.voidedCheck.files.length > 0))) });
           }).catch((e) => {
             authCheck(this.props.dispatch)(e)(this.props.location.pathname);
             this.setState({ accountDetails: {} });
           }
          );
  }
  openPlaidWidget=() => {
    this.handler.open();
  }
  toggleCollapse() {
    if (this.state.collapse === '') {
      this.setState({ collapse: 'in', collapseArrow: 'collapsed' });
    } else {
      this.setState({ collapse: '', collapseArrow: '' });
    }
  }
  render() {
    if (!this.state || !this.state.accountDetails) {
      return <Spinner />;
    }
    const { formatMessage } = this.props.intl;
    return (
      <div>
        {!this.state.hideMessage && <div className="alert alert-success alert-dismissable fade in">
          <i className="fa fa-check-square-o sucess_message" aria-hidden="true"></i>
          <strong>You are almost complete, please proceed to bank verification</strong>
        </div>}
        <div className="panel panel-default">
          <div className="panel-heading">
            <h4 className="panel-title">
              <Link className={`accordion-toggle ${this.state.collapseArrow}`} data-toggle="collapse" data-parent="#accordion" onClick={() => { this.toggleCollapse(); }}>
                <div className="activity-icon bg-success small"><i className="fa fa-bank"></i></div>{formatMessage(messages.header)}</Link>
            </h4>
          </div>
          <div id="collapseTwo" className={`panel-collapse collapse ${this.state.collapse}`}>
            <div className="panel-body">
              <h4>{formatMessage(messages.connectButtonHeader)}</h4>
              <ul>
                <li>Link your bank statements electronically (Recommended)</li>
                <li>Your Bank will automatically send us your statements</li>
                <li>Our automated underwriting system will review your statements for offers in less than 24 hours</li>
                <li>We use cutting-edge encryption technology to keep your information safe and secure</li>
              </ul>
              { !this.state.accountDetails.accountNumber &&
              <button onClick={this.openPlaidWidget} className="btn btn-default">{formatMessage(messages.connectButton)}</button>
            }
              { this.state.accountDetails && this.state.accountDetails.accountNumber &&
              <div>
                <h5>{formatMessage(messages.instituteName)} : {this.state.accountDetails.instituteName}</h5>
                <h5>{formatMessage(messages.accountNumber)} : {`*******${this.state.accountDetails.accountNumber}`}</h5>
                <h5>{formatMessage(messages.accountName)} : {this.state.accountDetails.accountName}</h5>
                <h5>{formatMessage(messages.type)} : {this.state.accountDetails.type}</h5>
              </div>
            }
            </div>
            <div className="bankLinkingOr">
              <p>OR</p>
              <h4>Manual Review</h4>
              <ul>
                <li>Upload or send us your bank statements</li>
                <li>This process is typically slower as you have to fax or upload your documents</li>
                <li>We then manually go through your documents nad make a decision</li>
              </ul>
              <h4>{formatMessage(messages.uploadThreeBankStatement)}</h4>
            </div>
            <Uploader mainHeader="" Stipulation={this.state.voidedCheck} uploadFileType={'other'} applicationNumber={this.state.applicationNumber} onDropSuccess={() => this.loadData()} showSpinner={false} />
          </div>
        </div>
      </div>
    );
  }
}

BankLink.propTypes = {
  applicationNumber: React.PropTypes.string.isRequired,
  intl: intlShape.isRequired,
  dispatch: React.PropTypes.func.isRequired,
  location: React.PropTypes.object.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapDispatchToProps)(withRouter(injectIntl(BankLink)));
