/**
*
* BusinessDetail
*
*/

import React from 'react';
import lodash from 'lodash';
import { Field, reset } from 'redux-form/immutable';
import Form from '@ui/Form';
import TextField from '@ui/TextField';
import Spinner from 'components/Spinner';
import { ein, sic } from 'lib/masks';
import decodeEntity from 'lib/decodeEntity';
import StateField from '@ui/StateField';
import ZipField from '@ui/ZipField';
import DateField from '@ui/DateField';
import localityNormalizer from '@ui/Utils/localityNormalizer';
import messages from './messages';

import validateForm from './validate';

const DropDownOptions = (obj) => (
  <option key={obj.key} value={obj.key}>{decodeEntity(obj.value)}</option>
  );

const DropDownList = ({ input, label, options, type, meta: { touched, error, warning } }) => ( // eslint-disable-line
  <div className=" form-group">
    <label htmlFor="label3">{label}</label>
    <select className="select_fields" {...input} placeholder={label} type={type}>
      <option key="0"></option>
      {options && Object.entries(options).map(([key, value]) => ({ key, value })).map(DropDownOptions)}
    </select>
    {touched &&
        ((error && <span className="error_message">{error}</span>) ||
          (warning && <span>{warning}</span>))}
  </div>
);

const ButtonPanel = (props) => {
  const { submitting ,resetForm} = props; // eslint-disable-line
  return (
    <div className="form-btn pull-right">
      {submitting && <Spinner />}
      <input type="button" disabled={submitting} onClick={() => resetForm()} className="btn submit-btn" value="Cancel" />
      <span>&nbsp;</span>
      <input type="submit" disabled={submitting} className="btn submit-btn" value="Next" />
    </div>
  );
};
class BusinessDetail extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { showSpinner: false };
  }
  modifyJson = (values) => {
    const finalValues = {
      ...values,
    };
    // Remove null, undefined from payload object
    const payload = lodash.pickBy(this.props.payloadGenerator(finalValues, 'business'), lodash.identity);
    if (payload.BusinessTaxID) {
      payload.BusinessTaxID = payload.BusinessTaxID.replace('-', '');
    }
    payload.ClientIpAddress = this.props.ip;
    this.setState({ showSpinner: true });
    return payload;
  };

  afterSubmit=(err, data) => {
    this.setState({ showSpinner: false });
    this.props.updateStateAfterSubmit(err, data, 'business');
  }
  initialValues = () => {
    const returnValue = {};
    if (this.props.business) {
      return this.props.business;
    }
    return returnValue;
  };
  handleOnLoadError = () => {
  // do not add authCheck here
  };
  resetForm = () => {
    this.props.dispatch(reset('businessDetails'));
  };

  render() {
    const parsedMessages = localityNormalizer(messages);
    return (
      <section id="content2" className="tab-section">
        <Form
          passPropsTo="ButtonPanel"
          afterSubmit={(err, data) => this.afterSubmit(err, data)}
          initialValuesBuilder={() => this.initialValues()}
          payloadBuilder={(values) => this.modifyJson(values)}
          validate={(values) => validateForm(this, values)}
          name="businessDetails"
          action="api/ca/save-lead"
          onLoadError={this.handleOnLoadError}
        >
          <div className="comman_application_wraper">
              <div className="col-md-6 col-sm-6">
                <TextField className="form-control" name="legalCompanyName" {...parsedMessages.legalCompanyName} />
              </div>
              <div className="col-md-6 col-sm-6">
                <TextField className="form-control" name="businessAddress" {...parsedMessages.businessAddress} />
              </div>
              <div className="col-md-6 col-sm-6">
                <TextField className="form-control" name="city" {...parsedMessages.city} />
              </div>
              <div className="col-md-6 col-sm-6">
                <StateField className="form-control" name="state" {...parsedMessages.state} />
              </div>
              <div className="col-md-6 col-sm-6">
                <ZipField className="form-control" name="postalCode" {...parsedMessages.postalCode} />
              </div>
              <div className="col-md-6 col-sm-6">
                <div className=" form-group">
                  <Field label="Rent Or Own this location?" name="homeOwnerType" options={this.props.lookup.PropertyType} component={DropDownList} type="select" />
                </div>
              </div>
              <div className="col-md-6 col-sm-6">
                <div className=" form-group">
                  <Field label="Industry" name="industry" options={this.props.lookup.Industry} component={DropDownList} type="select" />
                </div>
              </div>
              <div className="col-md-6 col-sm-6">
                <TextField mask={ein} className="form-control" name="buisnessTaxId" {...parsedMessages.buisnessTaxId} />
              </div>
              <div className="col-md-6 col-sm-6">
                <TextField mask={sic} className="form-control" name="sicCode" {...parsedMessages.sicCode} />
              </div>
              <div className="col-md-6 col-sm-6">
                <div className=" form-group">
                  <Field label="Entity Type" name="entityType" options={this.props.lookup.BusinessTypes} component={DropDownList} type="select" />
                </div>
              </div>
              {/* <div className="col-md-6 col-sm-6">
                <div className=" form-group">
                  <Field label="Address Type" name="addressType" options={this.props.lookup.AddressType} component={DropDownList} type="select" />
                </div>
              </div>*/}
              <div className="col-md-6 col-sm-10">
                <DateField
                  dateFormat="mm/dd/yyyy"
                  name="dateEstablished"
                  maxYears={100}
                  {...parsedMessages.dateOfBirth}
                />
              </div>
              <div className="col-md-6 col-sm-6">{this.state.showSpinner && <Spinner />}</div>
              <ButtonPanel resetForm={this.resetForm} />
          </div>
        </Form>
      </section>
    );
  }
}

BusinessDetail.propTypes = {
  dispatch: React.PropTypes.func.isRequired,
  payloadGenerator: React.PropTypes.any,
  business: React.PropTypes.any,
  lookup: React.PropTypes.any,
  updateStateAfterSubmit: React.PropTypes.any,
  ip: React.PropTypes.string,
};

export default BusinessDetail;
