/*
 * BusinessDetail Messages
 *
 * This contains all the text for the BusinessDetail component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.BusinessDetail.header',
    defaultMessage: 'This is the BusinessDetail component !',
  },
  legalCompanyNameLabel: {
    id: 'app.components.BusinessDetail.legalCompanyNameLabel',
    defaultMessage: 'Legal Company Name',
  },
  legalCompanyNamePlaceholder: {
    id: 'app.components.BusinessDetail.legalCompanyNamePlaceholder',
    defaultMessage: 'Legal Company Name',
  },
  businessAddressLabel: {
    id: 'app.components.BusinessDetail.businessAddressLabel',
    defaultMessage: 'Business Address',
  },
  businessAddressPlaceholder: {
    id: 'app.components.BusinessDetail.businessAddressPlaceholder',
    defaultMessage: 'Business Address',
  },
  cityLabel: {
    id: 'app.components.BusinessDetail.cityLabel',
    defaultMessage: 'City',
  },
  cityPlaceholder: {
    id: 'app.components.BusinessDetail.cityPlaceholder',
    defaultMessage: 'City',
  },
  stateLabel: {
    id: 'app.components.BusinessDetail.stateLabel',
    defaultMessage: 'State',
  },
  statePlaceholder: {
    id: 'app.components.BusinessDetail.statePlaceholder',
    defaultMessage: 'State',
  },
  postalCodeLabel: {
    id: 'app.components.BusinessDetail.postalCodeLabel',
    defaultMessage: 'Postal Code',
  },
  postalCodePlaceholder: {
    id: 'app.components.BusinessDetail.postalCodePlaceholder',
    defaultMessage: 'Postal Code',
  },
  buisnessTaxIdLabel: {
    id: 'app.components.BusinessDetail.buisnessTaxIdLabel',
    defaultMessage: 'Buisness Tax ID',
  },
  buisnessTaxIdPlaceholder: {
    id: 'app.components.BusinessDetail.buisnessTaxIdPlaceholder',
    defaultMessage: 'Buisness Tax ID',
  },
  sicCodeLabel: {
    id: 'app.components.BusinessDetail.sicCodeLabel',
    defaultMessage: 'SIC Code',
  },
  sicCodePlaceholder: {
    id: 'app.components.BusinessDetail.sicCodePlaceholder',
    defaultMessage: 'SIC Code',
  },
  dateOfBirthLabel: {
    id: 'app.components.BusinessDetail.dateOfBirthLabel',
    defaultMessage: 'Date Established',
  },
});
