/*
 * SignDocument Messages
 *
 * This contains all the text for the SignDocument component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.SignDocument.header',
    defaultMessage: 'Sign Documents',
  },
  resendDocuSignEmail: {
    id: 'app.components.SignDocument.resendDocuSignEmail',
    defaultMessage: 'Resend Email',
  },
});
