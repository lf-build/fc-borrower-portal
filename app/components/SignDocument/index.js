/**
*
* SignDocument
*
*/

import React from 'react';
import { Link, withRouter } from 'react-router';
import { connect } from 'react-redux';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import authCheck from 'components/Helper/authCheck';
import Spinner from 'components/Spinner';
import AlertMessage from 'components/AlertMessage';
import DocuSign from 'assets/images/docuSign01.png';
import { injectIntl, intlShape } from 'react-intl';
import messages from './messages';

class SignDocument extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { applicationNumber: this.props.applicationNumber, isVisible: false, collapse: '', collapseArrow: '' };
  }
  componentDidMount() {
    this.loadData();
  }
  loadData() {
    return execute(undefined, undefined, ...'api/application-document/docu-sign'.split('/'), {
      applicationNumber: this.state.applicationNumber,
    })
    .then(({ body }) => {
      this.setState({ docuSign: body });
    }).catch((e) => {
      authCheck(this.props.dispatch)(e)(this.props.location.pathname);
    });
  }
  resendDocuSignMail() {
    execute(undefined, undefined, ...'api/agreement/resend-docusign'.split('/'), {
      applicationNumber: this.state.applicationNumber,
    })
    .then(() => {
      this.setState({ isError: false, message: 'Email sent successfully', isVisible: true });
    }).catch((e) => {
      authCheck(this.props.dispatch)(e)(this.props.location.pathname);
      this.setState({ isError: true, message: 'Error sending mail', isVisible: true });
    }
    ); // eslint-disable-line
  }
  toggleCollapse() {
    if (this.state.collapse === '') {
      this.setState({ collapse: 'in', collapseArrow: 'collapsed' });
    } else {
      this.setState({ collapse: '', collapseArrow: '' });
    }
  }
  render() {
    if (!this.state || !this.state.docuSign) {
      return <Spinner />;
    }
    const { docuSignStatus, docuSignVisible } = this.state.docuSign;
    const { formatMessage } = this.props.intl;
    if (docuSignVisible === false) {
      return (<span />);
    }
    return (
      <div className="panel panel-default">
        <div className="panel-heading">
          <h4 className="panel-title">
            <Link className={`accordion-toggle ${this.state.collapseArrow}`} data-toggle="collapse" data-parent="#accordion" onClick={() => { this.toggleCollapse(); }}>
              <div className="activity-icon bg-success small"><i className="fa fa-pencil-square-o  "></i></div>{formatMessage(messages.header)}</Link>
          </h4>
        </div>
        <div id="collapseThree" className={`panel-collapse collapse ${this.state.collapse}`}>
          <div className="panel-body">
            <div className="doc-status">
              <div className="doc-img">
                <img src={DocuSign} alt="" />
                {docuSignStatus === 'Unsigned' && <button className="btn btn-default pull-right" onClick={() => this.resendDocuSignMail()}>{formatMessage(messages.resendDocuSignEmail)}</button>}
              </div>
              <div className="status-d">
                <h4>Status: {docuSignStatus}</h4>
              </div>
              {this.state.isVisible ? (() => <span><br /><AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} /></span>)() : <span />}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

SignDocument.propTypes = {
  applicationNumber: React.PropTypes.string.isRequired,
  intl: intlShape.isRequired,
  dispatch: React.PropTypes.func.isRequired,
  location: React.PropTypes.object.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapDispatchToProps)(injectIntl(withRouter(SignDocument)));
