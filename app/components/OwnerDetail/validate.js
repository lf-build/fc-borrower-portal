import isInRange from 'lib/validation/isInRange';
import isRegexMatch from 'lib/validation/isRegexMatch';
import isValidAge from 'lib/validation/isValidAge';
import { decimalPlaces } from 'components/Helper/functions';
import { postalCodeRegex, emailRegex, mobileRegex, ssnRegex, dateRegex, textRegex } from 'lib/validation/regexList';

const validateForm = (that, _values) => {
  const errors = {};
  const values = _values.toJS();

  if (!values.owners || !values.owners.length) {
    errors.owners = { _error: 'At least one owner required' };
  } else {
    const ownerErrorsArray = [];
    values.owners.map((owner, index) => { //eslint-disable-line
      const ownerErrors = {};
       // Next button validations
      if (!owner || !owner.firstName) {
        ownerErrors.firstName = 'First Name is required';
      } else if (!isInRange(owner.firstName, 2, 100)) {
        ownerErrors.firstName = 'Field length must be between 2 and 100';
      } else if (!isRegexMatch(owner.firstName.trim(), textRegex)) {
        ownerErrors.firstName = 'Numbers or special characters not allowed';
      }

      if (!owner || !owner.lastName) {
        ownerErrors.lastName = 'Last Name is required';
      } else if (!isInRange(owner.lastName, 2, 100)) {
        ownerErrors.lastName = 'Field length must be between 2 and 100';
      } else if (!isRegexMatch(owner.lastName.trim(), textRegex)) {
        ownerErrors.lastName = 'Numbers or special characters not allowed';
      }
      if (!owner || !owner.mobilePhone) {
        ownerErrors.mobilePhone = 'Mobile Phone is required';
      } else if (!isRegexMatch(owner.mobilePhone.replace(/ /g, ''), mobileRegex)) {
        ownerErrors.mobilePhone = 'Invalid Primary Phone';
      }
      if (!owner || !owner.homeAddress) {
        ownerErrors.homeAddress = 'Home address is required';
      } else if (!isInRange(owner.homeAddress, 2, 100)) {
        ownerErrors.homeAddress = 'Field length must be between 2 and 100';
      }
      if (!owner || !owner.emailAddress) {
        ownerErrors.emailAddress = 'Email address is required';
      } else if (!isRegexMatch(owner.emailAddress, emailRegex)) {
        ownerErrors.emailAddress = 'Invalid Email Address';
      }
      if (!owner || !owner.city) {
        ownerErrors.city = 'City is required';
      } else if (!isInRange(owner.city, 2, 100)) {
        ownerErrors.city = 'Field length must be between 2 and 100';
      } else if (!isRegexMatch(owner.city.trim(), textRegex)) {
        ownerErrors.city = 'Numbers or special characters not allowed';
      }
      if (!owner || !owner.state) {
        ownerErrors.state = 'State is required';
      }
      if (!owner || !owner.postalCode) {
        ownerErrors.postalCode = 'Postal Code is required';
      } else if (!isRegexMatch(owner.postalCode, postalCodeRegex)) {
        ownerErrors.postalCode = 'Invalid Postal Code';
      }
      if (!owner || !owner.ownership) {
        ownerErrors.ownership = 'Ownership is required';
      } else if (isNaN(owner.ownership)) {
        ownerErrors.ownership = 'Ownership should be numeric value';
      } else if (decimalPlaces(owner.ownership) > 2) {
        ownerErrors.ownership = 'Ownership % accepts maximum 2 decimal places';
      } else if (values.owners.length > 1) {
        let totalOwnership = 0;
        values.owners.map((item) => {
          totalOwnership += Number(item.ownership);
          return true;
        });
        if (totalOwnership > 100) {
          ownerErrors.ownership = 'Cumulative ownership for all owners can not be more than 100%';
        }
      } else if (owner.ownership < 0) {
        ownerErrors.ownership = 'Ownership can not be negative';
      } else if (owner.ownership > 100) {
        ownerErrors.ownership = 'Ownership can not be more than 100%';
      }
      if (!owner || !owner.ssn) {
        ownerErrors.ssn = 'Social Security Number is required';
      } else if (!isRegexMatch(owner.ssn, ssnRegex)) {
        ownerErrors.ssn = 'Invalid Social Security Number';
      }
      if (!owner || !owner.dateOfBirth) {
        ownerErrors.dateOfBirth = 'Date of birth is required';
      } else if (!isRegexMatch(owner.dateOfBirth, dateRegex)) {
        ownerErrors.dateOfBirth = 'Invalid Date';
      } else if (!isValidAge(owner.dateOfBirth, 18)) {
        ownerErrors.dateOfBirth = 'Age must be greater than or equal to 18';
      } else {
        const parts = owner.dateOfBirth.split('/');
        const mydate = new Date(parts[2], parts[0] - 1, parts[1]);
        if (mydate >= new Date()) {
          ownerErrors.dateOfBirth = 'Date of birth can not be a future date';
        }
      }
      ownerErrorsArray[index] = ownerErrors;
    });
    if (ownerErrorsArray.length) {
      errors.owners = ownerErrorsArray;
    }
  }
  return errors;
};

export default validateForm;
