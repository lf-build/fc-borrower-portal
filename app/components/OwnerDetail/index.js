/**
*
* OwnerDetail
*
*/

import React from 'react';
import lodash from 'lodash';
import { FieldArray, reset } from 'redux-form/immutable';
import Form from '@ui/Form';
import Spinner from 'components/Spinner';
import TextField from '@ui/TextField';
import StateField from '@ui/StateField';
import PhoneField from '@ui/PhoneField';
import ZipField from '@ui/ZipField';
import SSNField from '@ui/SSNField';
import DateField from '@ui/DateField';
import EmailField from '@ui/EmailField';
import localityNormalizer from '@ui/Utils/localityNormalizer';
import messages from './messages';
import validateForm from './validate';

const parsedMessages = localityNormalizer(messages);

const ButtonPanel = (props) => {
  const { submitting ,resetForm} = props; // eslint-disable-line
  return (
    <div className="form-btn pull-right">
      {submitting && <Spinner />}
      <input type="button" disabled={submitting} onClick={() => resetForm()} className="btn submit-btn" value="Cancel" />
      <span>&nbsp;</span>
      <input type="submit" disabled={submitting} className="btn submit-btn" value="Next" />
    </div>
  );
};

class OwnerDetail extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { activeIndex: 0, isDisplay: true, showSpinner: false };
    this.tabs = {};
  }
  componentDidMount() {
  }

  RenderOwner = ({ fields, activeIndex }) => (
    <div> <div className="form-btn ">
      {
        fields.length === 4 ? null :
        <input type="button" onClick={() => fields.push({})} className="btn submit-btn add-another" value="+ Add another owner" />
      }
    </div>
      {fields.map((owner, index) =>
        <div key={index} className={activeIndex === index ? '' : ''}>
          <div className="form-btn border-top">
            <h4 className="heading-text">Owner {index + 1}</h4>
            {index === 0 ? null :
            <input onClick={() => fields.remove(index)} className="btn submit-btn add-another" value="Remove" type="button" />
          }
          </div>
            <div className="col-md-6 col-sm-6">
              <TextField className="form-control" name={`${owner}.firstName`} {...parsedMessages.firstName} />
            </div>
            <div className="col-md-6 col-sm-6">
              <TextField className="form-control" name={`${owner}.lastName`} {...parsedMessages.lastName} />
            </div>
            <div className="col-md-6 col-sm-6">
              <TextField className="form-control" name={`${owner}.homeAddress`} {...parsedMessages.homeAddress} />
            </div>
            <div className="col-md-6 col-sm-6">
              <TextField className="form-control" name={`${owner}.city`} {...parsedMessages.city} />
            </div>
            <div className="col-md-6 col-sm-6">
              <StateField className="form-control" name={`${owner}.state`} {...parsedMessages.state} />
            </div>
            <div className="col-md-6 col-sm-6">
              <ZipField className="form-control" name={`${owner}.postalCode`} {...parsedMessages.postalCode} />
            </div>
            <div className="col-md-6 col-sm-6">
              <PhoneField className="form-control" name={`${owner}.mobilePhone`} {...parsedMessages.mobilePhone} />
            </div>
            <div className="col-md-6 col-sm-6">
              <TextField className="form-control" name={`${owner}.ownership`} {...parsedMessages.ownership} />
            </div>
            <div className="col-md-6 col-sm-6">
              <SSNField className="form-control" name={`${owner}.ssn`} {...parsedMessages.ssn} />
            </div>
            <div className="col-md-6 col-sm-6">
              <EmailField className="form-control" name={`${owner}.emailAddress`} {...parsedMessages.emailAddress} />
            </div>
            <div className="col-md-6 col-sm-10">
              <DateField
                dateFormat="mm/dd/yyyy"
                name={`${owner}.dateOfBirth`}
                maxYears={100}
                {...parsedMessages.dateOfBirth}
              />
            </div>
        </div>
       )}
    </div>
 );
  modifyJson = (values) => {
    const finalValues = {
      ...values,
    };
    finalValues.owners = values.owners.map((owner) => {
      const finalOwner = owner;
      if (owner && owner.ssn) {
        finalOwner.ssn = owner.ssn.replace(/-/g, '');
      }
      if (owner && owner.mobilePhone) {
        finalOwner.mobilePhone = owner.mobilePhone.replace('(', '').replace(')', '').replace(/-/g, '').replace(/ /g, '');
      }
      return finalOwner;
    });
    // Remove null, undefined from payload object
    const payload = lodash.pickBy(this.props.payloadGenerator(finalValues, 'owner'), lodash.identity);
    const owners = payload.Owners.map((item) => lodash.pickBy(item, lodash.identity));
    // delete payload.owners;
    payload.Owners = owners;
    if (payload.BusinessTaxID) {
      payload.BusinessTaxID = payload.BusinessTaxID.replace('-', '');
    }
    payload.ClientIpAddress = this.props.ip;
    this.state.showSpinner = true;
    this.setState(this.state);
    return payload;
  };

  afterSubmit=(err, data) => {
    this.state.showSpinner = false;
    this.setState(this.state);
    this.props.updateStateAfterSubmit(err, data, 'owner');
  };
  initialValues = () => {
    const returnValue = { owners: [{}] };
    if (this.props.owner && this.props.owner.owners) {
      return this.props.owner;
    }
    return returnValue;
  };
  handleOnLoadError = () => {
   // do not add authCheck here
  };
  resetForm = () => {
    this.props.dispatch(reset('ownerDetails'));
  };
  render() {
    return (
      <section id="content3" className="tab-section">
        <Form
          passPropsTo="ButtonPanel"
          afterSubmit={(err, data) => this.afterSubmit(err, data)}
          initialValuesBuilder={(value) => this.initialValues(value)}
          payloadBuilder={(values) => this.modifyJson(values)}
          validate={(values) => validateForm(this, values)}
          name="ownerDetails"
          action="api/ca/save-lead"
          onLoadError={this.handleOnLoadError}
        >
          <div className="comman_application_wraper">
            <FieldArray addMember={this.addMember} isDisplay={this.state.isDisplay} removeMember={this.removeMember} activeIndex={this.state.activeIndex} tabClick={this.tabClick} name="owners" component={this.RenderOwner} />
              <div className="col-md-6 col-sm-6">{this.state.showSpinner && <Spinner />}</div>
              <ButtonPanel resetForm={this.resetForm} />
          </div>
        </Form>
      </section>
    );
  }
}

OwnerDetail.propTypes = {
  dispatch: React.PropTypes.func.isRequired,
  payloadGenerator: React.PropTypes.any,
  owner: React.PropTypes.any,
  updateStateAfterSubmit: React.PropTypes.any,
  ip: React.PropTypes.string,
};

export default OwnerDetail;
