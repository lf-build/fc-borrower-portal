/**
*
* Sidenav
*
*/

import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import Spinner from 'components/Spinner';
import { getInitialName } from 'components/Helper/functions';
import makeAuthSelector from 'sagas/auth/selectors';

class Sidenav extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    if (!this.props.auth || !this.props.auth.user) {
      // browserHistory.replace('/home');
      return <Spinner />;
    }
    const { auth: { user } } = this.props;
    const initialName = getInitialName(user.userName);
    return (
      <aside className="fixed">
        <div className="slimScrollDiv">
          <div className="sidebar-inner scrollable-sidebar">
            <div className="user-block clearfix">
              {user && user.image && <img src={`${user.image}?${Math.random()}`} alt={initialName} />}
              {user && !user.image && <span className="b-logo"><span className="logo-text">{initialName}</span></span>}
              <div className="detail"><p>Welcome</p><strong>{user.userName}</strong></div>
            </div>
            <div className="main-menu">
              <ul>
                <li className="active">
                  <Link to="/overview">
                    <span className="menu-icon">
                      <i className="fa fa-user  fa-lg"></i>
                    </span>
                    <span className="text">Overview</span>
                  </Link>
                </li>
                <li className="active">
                  <Link to="/todo">
                    <span className="menu-icon">
                      <i className="fa fa-check-square-o  fa-lg"></i>
                    </span>
                    <span className="text">
                      {'To Do\'s'}
                    </span>
                  </Link>
                </li>
                <li className="active">
                  <Link to="/profile">
                    <span className="menu-icon">
                      <i className="fa fa-user  fa-lg"></i>
                    </span>
                    <span className="text">Profile</span>
                  </Link>
                </li>
                <li className="active">
                  <Link to="/change-password">
                    <span className="menu-icon">
                      <i className="fa fa-lock   fa-lg"></i>
                    </span>
                    <span className="text">Change Password </span>
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </aside>
    );
  }
}

Sidenav.propTypes = {
  auth: React.PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  auth: makeAuthSelector(),
});

export default connect(mapStateToProps)(Sidenav);
