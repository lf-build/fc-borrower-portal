/**
*
* Faqs
*
*/

import React from 'react';
import appSecure from 'assets/images/app-secure.png';
import arating from 'assets/images/arating.png';
// import styled from 'styled-components';

class Faqs extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div className="col-sm-3">
        <div className="right-images">
          <a><img alt="" src={appSecure} /></a>
        </div>
        <div className="questions">
          <h3>Questions?</h3>
          <p>{'We\'re here to answer any questions or take your application over the phone.'}</p>
          <div className="que-detail">
            <h4>899-328-9911</h4>
            <a>Email Support</a>
            <a>Chat Live</a>
          </div>
        </div>
        <div className="about-all">
          <h3>About Bapital Alliance</h3>
          <p>Capital Alliance was built by the same drive and passion that fuels our clients. We offer simple business loans up to $1 million.</p>
        </div>
        <div className="rating">
          <a><img alt="" src={arating} /></a>
        </div>
      </div>
    );
  }
}

Faqs.propTypes = {

};

export default Faqs;
