/*
 * Faqs Messages
 *
 * This contains all the text for the Faqs component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Faqs.header',
    defaultMessage: 'This is the Faqs component !',
  },
});
