import { createSelector } from 'reselect';

/**
 * Direct selector to the finalization state domain
 */
const selectFinalizationDomain = () => (state) => state.get('finalization');

/**
 * Other specific selectors
 */


/**
 * Default selector used by Finalization
 */

const makeSelectFinalization = () => createSelector(
  selectFinalizationDomain(),
  (substate) => substate.toJS()
);

export default makeSelectFinalization;
export {
  selectFinalizationDomain,
};
