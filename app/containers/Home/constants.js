/*
 *
 * Home constants
 *
 */

export const DEFAULT_ACTION = 'app/Home/DEFAULT_ACTION';
export const DETECT_IP_ADDRESS = 'app/Home/DETECT_IP_ADDRESS';
export const IP_DETECTED_SUCCESS = 'app/Home/IP_DETECTED_SUCCESS';
export const IP_DETECTED_FAILED = 'app/Home/IP_DETECTED_FAILED';
