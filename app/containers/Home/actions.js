/*
 *
 * Home actions
 *
 */

import {
  DEFAULT_ACTION,
  DETECT_IP_ADDRESS,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function detectIP() {
  return {
    type: DETECT_IP_ADDRESS,
  };
}
