FROM borrower-base
ADD /package.json /tmp/package.json
WORKDIR /tmp
RUN npm install
RUN rm -f .npmrc
EXPOSE 3000
{files}
ENTRYPOINT npm run start