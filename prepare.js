#!/usr/bin/env node
const glob = require("glob");
const { 
    readFileSync,
    writeFileSync
} = require('fs');

glob("./app/**/*.*", {}, function (er, files) {
    const a = files
    .filter(f => !f.startsWith('_'))
    .map(f => `COPY ${f} ${f.replace('./', '/tmp/')}`)
    .reduce((a, c) => `${a}\n${c}`);
    writeFileSync('Dockerfile', readFileSync('prod.template.Dockerfile', 'utf8').replace(/{files}/g, a));
  });